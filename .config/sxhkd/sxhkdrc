# reloads sxhkd configuration:
super + Escape
    pkill -USR1 -x sxhkd

# Lock Screen
ctrl + alt + l
    i3lock-fancy

# Reload polybar
super + shift + r
	$HOME/.config/polybar/launch.sh


################
# ROFI COMMANDS
################

# App Launcher
super + space
#    rofi -show drun
	rofi -show drun -config .config/bspwm/rofi/launchers/config.rasi	

# Windows Launcher
super + w
	rofi -show window -config .config/bspwm/rofi/launchers/config.rasi

# Launch Menus
super + x
	~/.config/bspwm/rofi/powermenu/powermenu.sh

	 
################
# LAUNCH APPLICATIONS
################

# Launch Terminal
super + Return
    alacritty

super + shift + {f,w,c}
    {nemo,firefox,code-oss}

# Volume controls
XF86Audio{RaiseVolume,LowerVolume,Mute}
    $HOME/.local/bin/volume{ --up, --down, --toggle}

# Brightness controls
XF86MonBrightness{Up,Down}
    $HOME/.local/bin/brightness{ --up, --down}

# screenshot
Print
    flameshot launcher


################
# BSPWM HOTKEYS
################

# quit/restart bspwm
ctrl + shift + {q,r}
    bspc {quit,wm -r}

# close and kill
super + q
    bspc node -c

# alternate between the tiled and monocle layout
#ctrl + space
#	bspc node -t "~"{floating,tiled}


#NEED TO CHECK THESE WORKING UP UNTIL ~~~~
# Tiled mode
super + t
    if [ -z "$(bspc query -N -n focused.tiled)" ]; then \
        bspc node focused -t tiled; \
	    notify-send -u low -t 800 "Tiled"; \
    else \
	    notify-send -u normal -t 800 "Already tiled"; \
    fi

# Toggle between pseudo tiled and tiled
super + y
    if [ -z "$(bspc query -N -n focused.pseudo_tiled)" ]; then \
        bspc node focused -t pseudo_tiled; \
	    notify-send -u low -t 800 "Pseudo tiled"; \
    else \
        bspc node focused -t tiled; \
	    notify-send -u low -t 800 "Undo tiled"; \
    fi

# Toggle between floating and tiled
super + f
    if [ -z "$(bspc query -N -n focused.floating)" ]; then \
        bspc node focused -t floating; \
	    notify-send -u low -t 800 "Floating"; \
    else \
        bspc node focused -t tiled; \
	    notify-send -u low -t 800 "Undo floating"; \
    fi

# Toggle between fullscreen and tiled
super + g
    if [ -z "$(bspc query -N -n focused.fullscreen)" ]; then \
        bspc node focused -t fullscreen; \
	    notify-send -u low -t 800 "Fullscreen"; \
    else \
        bspc node focused -t tiled; \
	    notify-send -u low -t 800 "Undo fullscreen"; \
    fi


# CHECK TO HERE ~~~~~


# send the newest marked node to the newest preselected node
#super + y
#	bspc node newest.marked.local -n newest.!automatic.local

# swap the current node and the biggest window
#super + g
#	bspc node -s biggest.window


################
# state/flags
################

# set the window state
super + {t,ctrl + t,f}
	bspc node -t {tiled,pseudo_tiled,fullscreen}

# set the node flags
super + ctrl + {m,x,y,z}
	bspc node -g {marked,locked,sticky,private}

# Send the window to another edge of the screen
super + {_,shift + }{Left,Down,Up,Right}
    bspc node -{f,s} {west,south,north,east}

# Change focus to next window, including floating window
alt + {_,shift + }Tab
    bspc node -f {next.local,prev.local}
 
################
# focus/swap
################

# focus the node in the given direction
super + {_,shift + }{h,j,k,l}
	bspc node -{f,s} {west,south,north,east}

# focus the node for the given path jump
super + {p,b,comma,period}
	bspc node -f @{parent,brother,first,second}

# focus the next/previous window in the current desktop
super + {_,shift + }c
	bspc node -f {next,prev}.local.!hidden.window

# focus the next/previous desktop in the current monitor
super + bracket{left,right}
	bspc desktop -f {prev,next}.local
	
# focus the last node/desktop
super + {grave,Tab}
	bspc {node,desktop} -f last

# focus the older or newer node in the focus history
super + {o,i}
	bspc wm -h off; \
	bspc node {older,newer} -f; \
	bspc wm -h on

# focus or send to the given desktop
super + {_,shift + }{1-9,0}
	bspc {desktop -f,node -d} '^{1-9,10}'


# Swap windows {Left,Right,Up,Down}
super + alt + {Left,Right,Up,Down}
	bspc node --swap {west,east,north,south}

################
# preselect
################

# preselect the direction
super + ctrl + {h,j,k,l}
	bspc node -p {west,south,north,east}

# preselect the ratio
super + ctrl + {1-9}
	bspc node -o 0.{1-9}

# cancel the preselection for the focused node
super + ctrl + space
	bspc node -p cancel

# cancel the preselection for the focused desktop
super + ctrl + shift + space
	bspc query -N -d | xargs -I id -n 1 bspc node id -p cancel

################
# MOVE/RESIZE
################

## Expanding windows
ctrl + super + {Left,Down,Up,Right}
    $HOME/.config/bspwm/sxhkd/resize.sh {left,down,up,right}

# move a floating window
ctrl + alt {Left,Down,Up,Right}
	bspc node -v {-20 0,0 20,0 -20,20 0}

## Cycle between workspaces ##
#super + bracket{left,right}
#  	bspc desktop -f {prev,next}.local

super + bracket{left,right}
	bspc desktop -f {prev.occupied,next.occupied}

